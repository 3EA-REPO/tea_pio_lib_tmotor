# README #

This library allows you to read data from the TMotor ESCs and is based on information found here:

https://wiki.paparazziuav.org/wiki/Alpha_esc_with_telemetry_output

The function readData() needs to be called continuously to keep reading bytes from the ESC. 
Once a whole data frame has been assembled, all data fields are updated and can be read using the according getter function.
#include <Arduino.h>
#include <Tea_TMotor.hpp>

TEA_TMOTOR myTMotor;

void setup() {
  Serial.begin(115200);
  myTMotor.init();
}

void loop() {
  myTMotor.readData();
  Serial.println(myTMotor.get_actual_throttle(0));
}

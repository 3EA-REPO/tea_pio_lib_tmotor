#include "Tea_TMotor.hpp"

TEA_TMOTOR::TEA_TMOTOR()
{
}

void TEA_TMOTOR::init()
{
    TMotor_serial.begin(115200);
}

void TEA_TMOTOR::readData()
{
    // a four byte array (frameHeaderSearcher) is constantly
    // filled and contains the last four received bytes.
    // it is compared with a reference frame header. If a
    // frame header is found by this method, the framePointer
    // gets reset, because we found the start of a message.

    while (TMotor_serial.available() > 0)
    {
        uint8_t c = TMotor_serial.read();
        //Serial.write(c);
        // check if the last 4 characters were a header
        if (headerFound(c))
        {
            frame[0] = frameHeaderSearcher[0];
            frame[1] = frameHeaderSearcher[1];
            frame[2] = frameHeaderSearcher[2];
            frame[3] = frameHeaderSearcher[3];
            framePointer = 3;
        }
        // fill byte into frame array
        frame[framePointer] = c;
        framePointer++;
        if (framePointer > 160)
        {
            Serial.println("Frame Error!");
            framePointer = 0;
        }
    }
    if (framePointer >= 159)
    {
        framePointer = 0;
        parseFrame();
    }
}

boolean TEA_TMOTOR::headerFound(uint8_t c)
{
    // shift the whole header array to the left and fill in new byte
    frameHeaderSearcher[0] = frameHeaderSearcher[1];
    frameHeaderSearcher[1] = frameHeaderSearcher[2];
    frameHeaderSearcher[2] = frameHeaderSearcher[3];
    frameHeaderSearcher[3] = c;

     // compare with reference header
    if (
        frameHeaderSearcher[0] == frameHeaderReference[0] &&
        frameHeaderSearcher[1] == frameHeaderReference[1] &&
        frameHeaderSearcher[2] == frameHeaderReference[2] &&
        frameHeaderSearcher[3] == frameHeaderReference[3])
    {
        return true;
    }
    else
        return false;
}

uint16_t TEA_TMOTOR::mergeBytes(uint8_t msb, uint8_t lsb)
{
    uint16_t result = ((uint16_t)msb << 8) | lsb;
    return result;
}

void TEA_TMOTOR::parseFrame()
{
    // TODO: implement CRC check to verify frame integrety

    for (uint8_t esc_channel = 0; esc_channel < 8; esc_channel++)
    {
        uint8_t offset = esc_channel * 19;
        RX_throttle[esc_channel] = (mergeBytes(frame[offset + 9], frame[offset + 10]) * 100.0) / 1024.0;
        actual_throttle[esc_channel] = (mergeBytes(frame[offset + 11], frame[offset + 12]) * 100.0) / 1024.0;
        // TODO: calculate electric RPM correctly. Is currently raw data!
        electric_RPM[esc_channel] = mergeBytes(frame[offset + 13], frame[offset + 14]);
        busbar_voltage[esc_channel] = (mergeBytes(frame[offset + 15], frame[offset + 16])) / 10;
        // ToDO: not sure if this is calculated correctly.
        busbar_current[esc_channel] = (mergeBytes(frame[offset + 17], frame[offset + 18])) / 64;
        // ToDO: not sure if this is calculated correctly.
        phase_line_current[esc_channel] = (mergeBytes(frame[offset + 19], frame[offset + 20])) / 64;
        // TODO: Add lookup table for mos and cap temperatures: https://wiki.paparazziuav.org/wiki/File:Alpha_ESC_Temperature_Lookup.jpg
        MOS_temperature[esc_channel] = frame[offset + 21];
        capacitor_temperature[esc_channel] = frame[offset + 22];
        status_code[esc_channel] = mergeBytes(frame[offset + 23], frame[offset + 24]);
    }

    // testing stuff below:

    /*
    // Print throttle of channel 1
    Serial.print("Throttle Channel " + String(frame[6]));
    float throttle = (mergeBytes(frame[9], frame[10]) * 100.0) / 1024.0;
    Serial.println(": " + String(throttle));
    */

    /*
    // Print entire frame in hex bytes
    Serial.print("0x");
    for (int i = 0;i <159;i++){
        Serial.print(frame[i],HEX);
        Serial.print(", 0x");
    }
    Serial.println(frame[159],HEX);
    */
}

float TEA_TMOTOR::get_RX_throttle(uint8_t esc_channel)
{
    return RX_throttle[esc_channel];
}
float TEA_TMOTOR::get_actual_throttle(uint8_t esc_channel)
{
    return actual_throttle[esc_channel];
}
uint16_t TEA_TMOTOR::get_electric_RPM(uint8_t esc_channel)
{
    return electric_RPM[esc_channel];
}
uint16_t TEA_TMOTOR::get_busbar_voltage(uint8_t esc_channel)
{
    return busbar_voltage[esc_channel];
}
int16_t TEA_TMOTOR::get_busbar_current(uint8_t esc_channel)
{
    return busbar_current[esc_channel];
}
int16_t TEA_TMOTOR::get_phase_line_current(uint8_t esc_channel)
{
    return phase_line_current[esc_channel];
}
uint8_t TEA_TMOTOR::get_MOS_temperature(uint8_t esc_channel)
{
    return MOS_temperature[esc_channel];
}
uint8_t TEA_TMOTOR::get_capacitor_temperature(uint8_t esc_channel)
{
    return capacitor_temperature[esc_channel];
}
uint16_t TEA_TMOTOR::get_status_code(uint8_t esc_channel)
{
    return status_code[esc_channel];
}

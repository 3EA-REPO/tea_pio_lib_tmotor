#ifndef TEA_TMOTOR_HPP
#define TEA_TMOTOR_HPP

#if ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

#define TMotor_serial Serial2

class TEA_TMOTOR
{

  public:

    TEA_TMOTOR();
    void init();
    void readData();
    float get_RX_throttle(uint8_t);
    float get_actual_throttle(uint8_t);
    uint16_t get_electric_RPM(uint8_t);
    uint16_t get_busbar_voltage(uint8_t);
    int16_t get_busbar_current(uint8_t);
    int16_t get_phase_line_current(uint8_t);
    uint8_t get_MOS_temperature(uint8_t);
    uint8_t get_capacitor_temperature(uint8_t);
    uint16_t get_status_code(uint8_t);

  private:

    void parseFrame();
    uint16_t mergeBytes(uint8_t, uint8_t);
    boolean headerFound(uint8_t);
    uint8_t frame[160];
    uint8_t framePointer = 0;
    uint8_t frameHeaderSearcher[4];
    // this is what each frame begins with.
    // 0x9B = frame head
    // 0x9E = frame length (160 Byte)
    // 0x01 = communication protocol version
    // 0x02 = command word
    const uint8_t frameHeaderReference[4] = {0x9B, 0x9E, 0x01, 0x02};
    // ESC Parameters
    float RX_throttle[8] = {.0,.0,.0,.0,.0,.0,.0,.0};
    float actual_throttle[8] = {.0,.0,.0,.0,.0,.0,.0,.0};
    uint16_t electric_RPM[8] = {0,0,0,0,0,0,0,0};
    uint16_t busbar_voltage[8] = {0,0,0,0,0,0,0,0};
    int16_t busbar_current[8] = {0,0,0,0,0,0,0,0};
    int16_t phase_line_current[8] = {0,0,0,0,0,0,0,0};
    uint8_t MOS_temperature[8] = {0,0,0,0,0,0,0,0};
    uint8_t capacitor_temperature[8] = {0,0,0,0,0,0,0,0};
    uint16_t status_code[8] = {0,0,0,0,0,0,0,0};
};
#endif
